<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
</head>
<body>
    <h1 >Calculator</h1>
    
    <?php 
    
    $num1 = $_POST["num1"] ?? "";
    $num2 = $_POST["num2"] ?? "";
    $operator = $_POST["operator"] ?? "";
    $err1 = "";
    $err2 = "";
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($num1 == "" || !is_numeric($num1)) {
            $err1 = "Number 1 is not a number";
        } 
        if ($num2 == "" || !is_numeric($num2)) {
            $err2 = "Number 2 is not a number";        
        }
        if ($num2 == "0" && $operator == "Divide") {
            $err2 = "Division by zero is not allowed";
        }
        if ($num2 == "0" && $operator == "Modulo") {
            $err2 = "Modulo zero is not allowed";
        }
    }  

    function calculate($num1, $num2, $operator)
    {       
        if (is_numeric($num1) && is_numeric($num2)) {
            switch ($operator) {
                case "Add":
                    echo $num1 + $num2;
                    break;
                case "Subtract":
                    echo $num1 - $num2;
                    break;
                case "Multiply":
                    echo $num1 * $num2;
                    break;
                case "Divide":
                    if ($num2 != 0) {
                        echo $num1 / $num2;
                    } 
                    break;
                case "Modulo":
                    echo fmod($num1, $num2);
                    break;
            }   
        }
    }

    ?>

    <form method="post">
        <label for="num1" style="font-weight: bold">Number 1:</label>
        <input type="text" name="num1" value="<?php echo $num1 ?>">
        <span style="color: red"><?php echo $err1 ?></span><br><br>

        <label for="num2" style="font-weight: bold">Number 2:</label>
        <input type="text" name="num2" value="<?php echo $num2 ?>">
        <!-- <span style="color: red"><?php echo $err2?></span>  -->
        <span style="color: red"><?php echo $err2?></span><br><br> 
        
        <?php 
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !$err1 && !$err2) {
            ?>
                
        <div>
            <p>Operation: <?php echo $operator; ?> </p>
            <p>Result: <?php echo calculate($num1, $num2, $operator); ?> </p>
        </div>
            
            <?php 
        }       
        ?>        

        <input type="submit" name="operator" value="Add">
        <input type="submit" name="operator" value="Subtract">
        <input type="submit" name="operator" value="Multiply">
        <input type="submit" name="operator" value="Divide">
        <input type="submit" name="operator" value="Modulo">
    </form>
        
</body>
</html>